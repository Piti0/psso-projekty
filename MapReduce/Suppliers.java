import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.ArrayWritable;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.KeyValueTextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

import java.io.IOException;
import java.util.*;

public class Suppliers extends Configured implements Tool {

    public static void main(String[] args) throws Exception {
        int res = ToolRunner.run(new Configuration(), new Suppliers(), args);
        System.exit(res);
    }
    public static class IntArrayWritable extends ArrayWritable {
        public IntArrayWritable() {
            super(IntWritable.class);
        }

        public IntArrayWritable(Integer[] numbers) {
            super(IntWritable.class);
            IntWritable[] ints = new IntWritable[numbers.length];
            for (int i = 0; i < numbers.length; i++) {
                ints[i] = new IntWritable(numbers[i]);
            }
            set(ints);
        }
    }
    public static class TextArrayWritable extends ArrayWritable {
        public TextArrayWritable() {
            super(Text.class);
        }

        public TextArrayWritable(String[] strings) {
            super(Text.class);
            Text[] texts = new Text[strings.length];
            for (int i = 0; i < strings.length; i++) {
                texts[i] = new Text(strings[i]);
            }
            set(texts);
        }
    }

    @Override
    public int run(String[] args) throws Exception {
        Configuration conf = this.getConf();
        FileSystem fs = FileSystem.get(conf);
        Path tmpPath = new Path("/mp2/tmp");//new Path(args[1]+"/tmp");
        fs.delete(tmpPath, true);

        Job jobA = Job.getInstance(conf, "Title Count");
        jobA.setOutputKeyClass(Text.class);
        jobA.setOutputValueClass(Text.class);

        jobA.setMapperClass(SuppliersCountMap.class);
        jobA.setReducerClass(SuppliersCountReduce.class);

        FileInputFormat.setInputPaths(jobA, new Path(args[0]));
        FileOutputFormat.setOutputPath(jobA, tmpPath);

        jobA.setJarByClass(Suppliers.class);
        jobA.waitForCompletion(true);

        Job jobB = Job.getInstance(conf, "Top Titles Statistics");
        jobB.setOutputKeyClass(Text.class);
        jobB.setOutputValueClass(Text.class);

        jobB.setMapOutputKeyClass(NullWritable.class);
        jobB.setMapOutputValueClass(TextArrayWritable.class);

        jobB.setMapperClass(TopSuppliersMap.class);
        jobB.setReducerClass(TopSuppliersReduce.class);
        jobB.setNumReduceTasks(1);

        FileInputFormat.setInputPaths(jobB, tmpPath);
        FileOutputFormat.setOutputPath(jobB, new Path(args[1]));

        jobB.setInputFormatClass(KeyValueTextInputFormat.class);
        jobB.setOutputFormatClass(TextOutputFormat.class);

        jobB.setJarByClass(Suppliers.class);
        return jobB.waitForCompletion(true) ? 0 : 1;
    }


    public static class SuppliersCountMap extends Mapper<Object, Text, Text, Text> {
        @Override
        public void map(Object key, Text value, Context context) throws IOException, InterruptedException {
            String text = value.toString();
            String[] texts=text.split(",");
            String company = texts[0].replace('"',' ').trim();
            String state = texts[2];
            context.write(new Text(company),new Text(state));
        }
    }

    public static class SuppliersCountReduce extends Reducer<Text, Text, Text, Text> {

        @Override
        public void reduce(Text key, Iterable<Text> values, Context context) throws IOException, InterruptedException {
            int count=0;
            Set<String> states = new HashSet<>();
            for(Text val:values){
                count++;
                states.add(val.toString());
            }

            context.write(key, new Text(count + "," + states.size()));
        }
    }

    public static class TopSuppliersMap extends Mapper<Text, Text, NullWritable, TextArrayWritable> {
        int N;
        private TreeSet<Pair<Integer, Pair<String, Integer>>> topSuppliers;

        @Override
        protected void setup(Context context) throws IOException,InterruptedException {
            Configuration conf = context.getConfiguration();

            String nString = conf.get("N");
            N = Integer.parseInt(nString);

            topSuppliers = new TreeSet<Pair<Integer, Pair<String, Integer>>>(new Comparator<Pair<Integer, Pair<String, Integer>>>() {
                @Override
                public int compare(Pair<Integer, Pair<String, Integer>> o1, Pair<Integer, Pair<String, Integer>> o2) {
                    return o1.getFirst().compareTo(o2.getFirst());
                }
            });
        }

        @Override
        public void map(Text key, Text value, Context context) throws IOException, InterruptedException {
            String company = key.toString();
            String[] strings = value.toString().split(",");
            Integer popularity=Integer.parseInt(strings[0].trim());
            Integer states = Integer.parseInt(strings[1].trim());;

            topSuppliers.add(new Pair<Integer, Pair<String, Integer>>(popularity, new Pair<String, Integer>(company, states)));

            if (topSuppliers.size() > N) {
                topSuppliers.remove(topSuppliers.first());
            }
        }

        @Override
        protected void cleanup(Context context) throws IOException, InterruptedException {
            for (Pair<Integer, Pair<String, Integer>> item : topSuppliers) {
                String[] strings = {item.first.toString(), item.second.first, item.second.getSecond().toString()};
                TextArrayWritable val = new TextArrayWritable(strings);
                context.write(NullWritable.get() ,val);
            }
        }
    }

    public static class TopSuppliersReduce extends Reducer<NullWritable, TextArrayWritable, Text, Text> {
        int N;
        private TreeSet<Pair<Integer, Pair<String, Integer>>> topSuppliers;

        @Override
        protected void setup(Context context) throws IOException,InterruptedException {
            Configuration conf = context.getConfiguration();

            String nString = conf.get("N");
            N = Integer.parseInt(nString);

            topSuppliers = new TreeSet<Pair<Integer, Pair<String, Integer>>>(new Comparator<Pair<Integer, Pair<String, Integer>>>() {
                @Override
                public int compare(Pair<Integer, Pair<String, Integer>> o1, Pair<Integer, Pair<String, Integer>> o2) {
                    return o1.getFirst().compareTo(o2.getFirst());
                }
            });
        }

        @Override
        public void reduce(NullWritable key, Iterable<TextArrayWritable> values, Context context) throws IOException, InterruptedException {
            for (TextArrayWritable val: values) {
                Text[] pair= (Text[]) val.toArray();

                Integer popularity = Integer.parseInt(pair[0].toString());
                String company = pair[1].toString();
                Integer states = Integer.parseInt(pair[2].toString());;

                topSuppliers.add(new Pair<Integer, Pair<String, Integer>>(popularity, new Pair<String, Integer>(company, states)));

                if (topSuppliers.size() > N) {
                    topSuppliers.remove(topSuppliers.first());
                }
            }

            List<Pair<Integer, Pair<String, Integer>>> items = new ArrayList<>();

            for (Pair<Integer, Pair<String, Integer>> item: topSuppliers) {
                items.add(item);
            }

            Collections.sort(items, new Comparator<Pair<Integer, Pair<String, Integer>>>() {
                @Override
                public int compare(Pair<Integer, Pair<String, Integer>> o1, Pair<Integer, Pair<String, Integer>> o2) {
                    return o1.second.second.compareTo(o2.second.second)*-1;
                }
            });

            for (Pair<Integer, Pair<String, Integer>> item: items) {
                context.write(new Text(item.second.first), new Text(item.first +"\t" + item.second.second));
            }
        }
    }

    static class Pair<A, B>{
        A first;
        B second;

        public Pair(A first, B second) {
            this.first = first;
            this.second = second;
        }

        public A getFirst() {
            return first;
        }

        public void setFirst(A first) {
            this.first = first;
        }

        public B getSecond() {
            return second;
        }

        public void setSecond(B second) {
            this.second = second;
        }
    }
}