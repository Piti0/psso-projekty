package RmiServer;

import java.rmi.RemoteException;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Singleton pattern
 */
public class AuctionServer implements IAuctionServer {
    private static AuctionServer instance = null;
    private ItemsContainer itemsContainer;
    private Map<String, Set<IAuctionListener>> listeners = new ConcurrentHashMap<>();
    private Thread workerThread;
    private Set<Item> nonExpiredItems = Collections.synchronizedSet(new HashSet<>());

    private AuctionServer() {
        itemsContainer = createItemsContainer();
    }

    public AuctionServer(ItemsContainer itemsContainer) {
        if (instance == null)
            instance = this;
        this.itemsContainer = itemsContainer;
        nonExpiredItems.addAll(Arrays.asList(itemsContainer.getAllItems()));
    }

    public static AuctionServer getAuctionServer() {
        if (instance == null)
            instance = new AuctionServer();
        return instance;
    }

    protected void runWorkerThread() {
        workerThread = new Thread(() -> {
            while (true) {
                try {
                    invokeListenersForExpiredItems();
                }catch(Exception e){}
            }
        });
        workerThread.start();
    }

    private void invokeListenersForExpiredItems() {
        Set<Item> items = new HashSet<>(nonExpiredItems);
        items.forEach(item -> {
            if (!item.isActual()) {
                invokeAllListeners(item);
                nonExpiredItems.remove(item);
            }
        });
    }

    private void invokeAllListeners(Item item) {
        listeners.forEach((key, value) -> {
            if (key.equals(item.getName()))
                value.forEach(iAuctionListener -> {
                    try {
                        iAuctionListener.update(item);
                    } catch (RemoteException e) {
                    }
                });
        });
    }

    protected ItemsContainer createItemsContainer() {
        return new CachedItemsContainer();
    }

    public void placeItemForBid(String ownerName, String itemName, String itemDesc, double startBid, int auctionTime) throws RemoteException {
        try {
            Item newItem = itemsContainer.putItem(ownerName, itemName, itemDesc, startBid, auctionTime);
            invokeAllListeners(newItem);
            addAllListenerToItem(newItem);
            nonExpiredItems.add(newItem);
        } catch (IllegalArgumentException e) {
            throw new RemoteException("Error", e);
        }
    }

    public void bidOnItem(String bidderName, String itemName, double bid) throws RemoteException {
        Item item = itemsContainer.getItem(itemName);
        if (item == null)
            throw new RemoteException("Item not found");
        if (!item.isActual())
            throw new RemoteException("Item expired");
        if (item.getBid() >= bid)
            throw new RemoteException("Incorrect bid action");
        item.updateBid(bid, bidderName);
    }

    public Item[] getItems() throws RemoteException {
        return itemsContainer.getAllItems();
    }

    public void registerListener(IAuctionListener al, String itemName) throws RemoteException {
        for (Item item : itemsContainer.getAllItems()) {
            if(itemName.equals(item.getName()))
                addListenerToItem(al, item);
        }

        if (!listeners.containsKey(itemName))
            listeners.put(itemName, Collections.newSetFromMap(new ConcurrentHashMap<>()));
        listeners.get(itemName).add(al);

    }

    private void addAllListenerToItem(Item item) {
        listeners.forEach((s, iAuctionListeners) -> {
            if(s.equals(item.getName())) {
                iAuctionListeners.forEach(obj -> addListenerToItem(obj, item));
            }
        });
    }

    private void addListenerToItem(IAuctionListener al, Item item) {
        //adding observer for observable
        item.addObserver((o, arg) -> {
            try {
                al.update((Item) o);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        });
    }
}

