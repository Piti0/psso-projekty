package RmiServer;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class LoggedDiskStoredItemsContainer extends DiskStoredItemsContainer {

    protected LoggedItem createItem(String owner, String itemName, String itemDesc, double bid, int auctionTime) {
        return new LoggedItem(owner, itemName, itemDesc, bid, auctionTime);
    }

    @Override
    public Item getItem(String name) {
        Item item = super.getItem(name);
        if(item!=null){
            LoggedItem loggedItem = new LoggedItem(item.getOwner(), item.getName(), item.getDesc(), item.getBid(), item.getAuctionTime());
            try {
                Method method = getClass().getSuperclass().getDeclaredMethod("applyObserver", Item.class);
                method.setAccessible(true);
                return (LoggedItem)method.invoke(this, loggedItem);
            } catch (NoSuchMethodException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

}
