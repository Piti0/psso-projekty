package RmiServer;

import java.io.Serializable;
import java.time.Instant;
import java.util.Date;
import java.util.Observable;

/**
 * Observer pattern - observable
 */
public class Item extends Observable implements Serializable {
    private String owner = null;
    private String name = null;
    private String desc = null;
    private double bid = 0;
    private int auctionTime = 0;
    private Date lastUpdate = Date.from(Instant.now());
    private String lastBidder=null;

    public Item() {
    }

    public Item(String owner, String name, String desc, double bid, int auctionTime) {
        this.owner = owner;
        this.name = name;
        this.desc = desc;
        this.bid = bid;
        this.auctionTime = auctionTime;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
        updateObservers();
    }

    private void updateObservers() {
        setChanged();
        notifyObservers();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
        updateObservers();
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
        updateObservers();
    }

    public double getBid() {
        return bid;
    }

    public void setBid(double bid) {
        this.bid = bid;
        lastUpdate = Date.from(Instant.now());
    }

    public int getAuctionTime() {
        return auctionTime;
    }

    public void setAuctionTime(int auctionTime) {
        this.auctionTime = auctionTime;
        updateObservers();
        lastUpdate = Date.from(Instant.now());
    }

    public Date getLastUpdate() {
        return lastUpdate;
    }

    public boolean isActual() {
        return Instant.now().getEpochSecond() - lastUpdate.toInstant().getEpochSecond() <= auctionTime;
    }

    public String getLastBidder() {
        return lastBidder;
    }

    public void setLastBidder(String lastBidder) {
        this.lastBidder = lastBidder;
    }

    public void updateBid(double bid, String bidder){
        setBid(bid);
        setLastBidder(bidder);
        lastUpdate = Date.from(Instant.now());
        updateObservers();
    }

    @Override
    public String toString() {
        return "Item{" +
                "owner='" + owner + '\'' +
                ", name='" + name + '\'' +
                ", desc='" + desc + '\'' +
                ", bid=" + bid +
                ", auctionTime=" + auctionTime +
                ", lastUpdate=" + lastUpdate +
                ", lastBidder='" + lastBidder + '\'' +
                ", isActual: "+isActual()+
                '}';
    }
}
