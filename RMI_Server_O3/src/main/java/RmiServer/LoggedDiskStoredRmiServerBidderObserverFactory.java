package RmiServer;

public class LoggedDiskStoredRmiServerBidderObserverFactory extends DiskStoredRmiServerBidderObserverFactory{

    @Override
    protected AuctionServer createAuctionServer() {
        LoggedDiskStoredItemsContainer t1=new LoggedDiskStoredItemsContainer();
        return new AuctionServerBidderObserver(t1);
    }

}
