package RmiServer;

import java.util.Observer;

public class LoggedItem extends Item{

    public LoggedItem(String owner, String name, String desc, double bid, int auctionTime) {
        super(owner, name, desc, bid, auctionTime);
        System.out.println("Item " + name + " was put on auction with starting bid: " + bid + " by: " + owner + " for: " + auctionTime);
        System.out.println("Description: " + desc);
    }

    public void updateBid(double bid, String bidder){
        super.updateBid(bid, bidder);
        System.out.println("Item " + this.getName() + " was bid for: " + bid + " by: " + bidder);
    }

    public void addObserver(Observer observer){
        super.addObserver(observer);
        System.out.println("Item " + this.getName() + " is observed");
    }

}
