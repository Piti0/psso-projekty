package RmiServer;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.FileVisitResult;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;

public aspect RmiServerFactoryA {
    private final String itemsDir = "./items/";

    pointcut createAuctionServer():
            call(AuctionServer RmiServer.RmiServerFactory.createAuctionServer());

    AuctionServer around():createAuctionServer(){
        Path dir = Paths.get(itemsDir);
        if (Files.isDirectory(dir)) {
            try {
                Files.walkFileTree(dir, new SimpleFileVisitor<Path>() {
                   @Override
                   public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
                       Files.delete(file);
                       return FileVisitResult.CONTINUE;
                   }

                   @Override
                   public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {
                       Files.delete(dir);
                       return FileVisitResult.CONTINUE;
                   }
                });
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        DiskStoredItemsContainer t1=new DiskStoredItemsContainer();
        return new AuctionServer(t1);
    }
}
