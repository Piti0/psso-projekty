package RmiServer;

public interface ItemsContainer {
    Item putItem(String owner, String itemName, String itemDesc, double bid, int auctionTime);

    Item[] getAllItems();

    Item getItem(String name);
}
