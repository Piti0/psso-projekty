package RmiServer;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;

@Aspect
public class ItemA {

    @After("call(RmiServer.Item.new(String, String, String, double, int))")
    public void logPuttingOnAuction(JoinPoint joinPoint) {
        String owner = (String) joinPoint.getArgs()[0];
        String itemName = (String) joinPoint.getArgs()[1];
        String description = (String) joinPoint.getArgs()[2];
        Double startingBid = (Double) joinPoint.getArgs()[3];
        int auctionTime = (int) joinPoint.getArgs()[4];

        System.out.println("Item " + itemName + " was put on auction with starting bid: " + startingBid + " by: " + owner + " for: " + auctionTime);
        System.out.println("Description: " + description);
    }

    @After("call(void RmiServer.Item.updateBid(double, String))")
    public void logBid(JoinPoint joinPoint) {
        Item biddedItem = (Item) joinPoint.getTarget();
        double bid = (Double) joinPoint.getArgs()[0];
        String bidder = (String) joinPoint.getArgs()[1];

        System.out.println("Item " + biddedItem.getName() + " was bid for: " + bid + " by: " + bidder);
    }

    @After("call(void RmiServer.Item.addObserver(..))")
    public void logMethods(JoinPoint joinPoint) {
        Item biddedItem = (Item) joinPoint.getTarget();

        System.out.println("Item " + biddedItem.getName() + " is observed");
    }

}
