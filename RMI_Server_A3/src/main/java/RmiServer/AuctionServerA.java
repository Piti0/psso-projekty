package RmiServer;

import com.google.gson.Gson;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

@Aspect
public class AuctionServerA {
    private final Path listPath;
    private Map<String, Double> stringDoubleMap = new ConcurrentHashMap<>();

    public AuctionServerA(){
        String uuid = UUID.randomUUID().toString();
        listPath=Paths.get("highScores"+uuid);
    }

    @After("call(void RmiServer.Item.updateBid(double, String))")
    public void logBidderHiggestBid(JoinPoint joinPoint) {
        Item biddedItem = (Item) joinPoint.getTarget();
        double bid = ((Double) joinPoint.getArgs()[0]);
        String bidder = (String) joinPoint.getArgs()[1];

        if (!stringDoubleMap.containsKey(bidder) || stringDoubleMap.get(bidder) < bid) {
            stringDoubleMap.put(bidder, bid);
            System.out.println("New higgest bid in history for: " + bidder + " value: " + bid + " strange item worth it: " + biddedItem.getName());
            String json = new Gson().toJson(stringDoubleMap);
            try {
                Files.write(listPath, json.getBytes());
            } catch (IOException e) {
            }
        }
    }
}
