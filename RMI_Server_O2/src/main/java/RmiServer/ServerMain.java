package RmiServer;

import java.io.IOException;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;

public class ServerMain {
    public static final String STUB_NAME = "Hello";

    public static void main(String args[]) {
        clearPreviousFiles();
        try {
            ServerFactory serverFactory = getCurrentContextServerFactory();
            IAuctionServer server = serverFactory.createAuctionServer(STUB_NAME);

            server.placeItemForBid("SomeOwner1", "Item1", "Opis", 10, 1000);
            server.placeItemForBid("SomeOwner2", "Item3", "Opis asdfasf", 100, 10000);
            server.placeItemForBid("SomeOwner1", "Item2", "Opis awe32352545343", 1, 500);

            System.err.println("Server ready");
        } catch (Exception e) {
            System.err.println("Server exception: " + e.toString());
            e.printStackTrace();
        }
    }

    protected static void clearPreviousFiles() {
        final String itemsDir = "./items/";

        Path dir = Paths.get(itemsDir);
        if (Files.isDirectory(dir)) {
            try {
                Files.walkFileTree(dir, new SimpleFileVisitor<Path>() {
                    @Override
                    public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
                        Files.delete(file);
                        return FileVisitResult.CONTINUE;
                    }

                    @Override
                    public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {
                        Files.delete(dir);
                        return FileVisitResult.CONTINUE;
                    }
                });
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private static RmiServerFactory getCurrentContextServerFactory() {
        return new DiskStoredRmiServerBidderObserverFactory();
    }
}
