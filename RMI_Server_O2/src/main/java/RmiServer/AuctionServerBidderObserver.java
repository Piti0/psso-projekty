package RmiServer;

import com.google.gson.Gson;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.rmi.RemoteException;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

public class AuctionServerBidderObserver extends AuctionServer {
    private final Path listPath;
    private Map<String, Double> stringDoubleMap = new ConcurrentHashMap<>();

    public AuctionServerBidderObserver(ItemsContainer itemsContainer) {
        super(itemsContainer);
        String uuid = UUID.randomUUID().toString();
        listPath = Paths.get("highScores" + uuid);
    }

    @Override
    public void bidOnItem(String bidderName, String itemName, double bid) throws RemoteException {
        super.bidOnItem(bidderName, itemName, bid);
        logBidderHiggestBid(bidderName, itemName, bid);
    }

    private void logBidderHiggestBid(String bidder, String biddedItem, double bid) {
        if (!stringDoubleMap.containsKey(bidder) || stringDoubleMap.get(bidder) < bid) {
            stringDoubleMap.put(bidder, bid);
            System.out.println("New higgest bid in history for: " + bidder + " value: " + bid + " strange item worth it: " + biddedItem);
            String json = new Gson().toJson(stringDoubleMap);
            try {
                Files.write(listPath, json.getBytes());
            } catch (IOException e) {
            }
        }
    }
}
