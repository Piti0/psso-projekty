package RmiServer;

import java.io.IOException;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;

public class DiskStoredRmiServerFactory extends RmiServerFactory{

    @Override
    protected AuctionServer createAuctionServer() {
        DiskStoredItemsContainer t1=new DiskStoredItemsContainer();
        return new AuctionServer(t1);
    }
}

