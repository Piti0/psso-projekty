package RmiServer;

import java.rmi.AlreadyBoundException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

/**
 * AbstractFactory implementation
 */
public class RmiServerFactory implements ServerFactory {

    public IAuctionServer createAuctionServer(String serviceName) throws Exception{
        AuctionServer obj = createAuctionServer();
        obj.runWorkerThread();
        IAuctionServer stub = makeStubObject(obj);
        registerStub(serviceName, stub);
        return stub;
    }

    protected void registerStub(String serviceName, IAuctionServer stub) throws RemoteException, AlreadyBoundException {
        Registry registry = LocateRegistry.getRegistry();
        try {
            registry.lookup(serviceName);
            registry.rebind(serviceName, stub);
        }
        catch (NotBoundException e) {
            registry.bind(serviceName, stub);
        }
    }

    protected IAuctionServer makeStubObject(AuctionServer obj) throws RemoteException {
        return (IAuctionServer) UnicastRemoteObject.exportObject(obj, 0);
    }

    protected AuctionServer createAuctionServer() {
        return AuctionServer.getAuctionServer();
    }
}

