package RmiServer;

public class DiskStoredRmiServerBidderObserverFactory extends RmiServerFactory{

    @Override
    protected AuctionServer createAuctionServer() {
        DiskStoredItemsContainer t1=new DiskStoredItemsContainer();
        return new AuctionServerBidderObserver(t1);
    }
}
