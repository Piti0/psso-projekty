package main.java.tree;

public interface TreeNode {

    String getLabel();

}
