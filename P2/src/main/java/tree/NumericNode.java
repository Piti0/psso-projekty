package main.java.tree;

public class NumericNode implements TreeNode {

    private int value;

    public NumericNode(int value){
        this.value = value;
    }

    @Override
    public String getLabel() {
        return Integer.toString(value);
    }

    public int getValue(){
        return value;
    }
}
