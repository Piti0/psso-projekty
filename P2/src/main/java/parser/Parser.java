package main.java.parser;

import java.util.Stack;
import java.util.StringTokenizer;

import main.java.tree.BinaryOperatorNode;
import main.java.tree.NumericNode;
import main.java.tree.TreeNode;

public class Parser {
    private StringTokenizer lexAnalyser;
    private ParserSettings parserSettings;
    private Stack<TreeNode> treeStack;
    private Stack<String> operatorStack;

    public Parser(ParserSettings parserSettings) {
        this.parserSettings = parserSettings;
    }

    public TreeNode parse(String inFixExpression) {
        treeStack = new Stack<>();
        operatorStack = new Stack<>();
        lexAnalyser = new StringTokenizer(inFixExpression);
        while (lexAnalyser.hasMoreTokens()) {
            String token = lexAnalyser.nextToken();
            if (parserSettings.isOpeningBracket(token)) {
                operatorStack.push(token);
            } else if (parserSettings.isValue(token)) {
                NumericNode tree = new NumericNode(Integer.parseInt(token));
                treeStack.push(tree);
            } else if (parserSettings.isOperator(token)) {
                if (operatorStack.empty() || parserSettings.isOpeningBracket(operatorStack.peek()) || parserSettings.getOperatorPriority(operatorStack.peek()) < parserSettings.getOperatorPriority(token)) {
                    operatorStack.push(token);
                } else { // clear operator stack and push new one onto it
                    do {
                        PopConnectPush();
                    }
                    while (!operatorStack.empty() && !parserSettings.isOpeningBracket(operatorStack.peek()) && parserSettings.getOperatorPriority(token) < parserSettings.getOperatorPriority(operatorStack.peek()));
                    operatorStack.push(token);
                }
            } else if (parserSettings.isClosingBracket(token)) {
                while (!parserSettings.isOpeningBracket(operatorStack.peek())) PopConnectPush();
                operatorStack.pop(); //Pop the opening bracket off the stack.
            } else {
                System.out.println("Error in expression");
            }
        }
        // no more tokens left in expression
        while (!operatorStack.empty()) {
            PopConnectPush();
        }
        // pointer to root of final tree is on top of the tree stack
        return treeStack.pop();
    }

    protected void PopConnectPush() {
        TreeNode right = treeStack.pop();
        TreeNode left = treeStack.pop();
        BinaryOperatorNode binaryOperatorNode = parserSettings.tryGetOperatorObject(operatorStack.pop(), left, right);
        treeStack.push(binaryOperatorNode);
    }
}
