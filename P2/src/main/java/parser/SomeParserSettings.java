package main.java.parser;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javafx.util.Pair;
import main.java.tree.BinaryOperatorNode;
import main.java.tree.TreeNode;
import main.java.tree.operatorNodes.AdditionNode;
import main.java.tree.operatorNodes.DivisionNode;
import main.java.tree.operatorNodes.MultiplicationNode;
import main.java.tree.operatorNodes.SubstractionNode;

public class SomeParserSettings implements ParserSettings {
    private List<Pair<String, Integer>> operators;
    private Map<String, Class> operatorsClasses = new HashMap<>();

    public SomeParserSettings() {
        initStandardData();
    }

    private void initStandardData(){
        operators = new ArrayList<>(Arrays.asList(
                new Pair<>("+", 1),
                new Pair<>("-", 1),
                new Pair<>("*", 2),
                new Pair<>("/", 2)));
        operatorsClasses.put("+", AdditionNode.class);
        operatorsClasses.put("-", SubstractionNode.class);
        operatorsClasses.put("*", MultiplicationNode.class);
        operatorsClasses.put("/", DivisionNode.class);
    }

    public void addOperator(String operator, int priority, Class operatorClass){
        operators.add(new Pair<>(operator, priority));
        operatorsClasses.put(operator, operatorClass);
    }

    @Override
    public BinaryOperatorNode tryGetOperatorObject(String operator, TreeNode left, TreeNode right) {
        BinaryOperatorNode binaryOperatorNode = null;
        try {
            Constructor<?> cons = operatorsClasses.get(operator).getConstructor(TreeNode.class, TreeNode.class);
            binaryOperatorNode = (BinaryOperatorNode)cons.newInstance(left, right);
        } catch (InstantiationException |
                IllegalAccessException |
                NoSuchMethodException |
                InvocationTargetException e) {
            e.printStackTrace();
        }
        return binaryOperatorNode;
    }

    @Override
    public boolean isClosingBracket(String token) {
        return token.equals(")");
    }

    @Override
    public boolean isOpeningBracket(String token) {
        return token.equals("(");
    }

    @Override
    public int getOperatorPriority(String op) {
        for (Pair<String, Integer> operator : operators)
            if (operator.getKey().equals(op))
                return operator.getValue();
        return -1;
    }

    @Override
    public boolean isValue(String integer) {
        try {
            Integer.parseInt(integer);
            return true;
        } catch (NumberFormatException nfe) {
            return false;
        }
    }

    @Override
    public boolean isOperator(String op) {
        for (Pair<String, Integer> operator : operators)
            if (operator.getKey().equals(op)) return true;
        return false;
    }
}
