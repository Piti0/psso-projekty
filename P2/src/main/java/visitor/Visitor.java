package main.java.visitor;

public interface Visitor {

	void visit(Node node);
	void visit(Leaf leaf);
	String report();
	
}
