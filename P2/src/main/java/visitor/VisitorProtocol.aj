package main.java.visitor;

privileged abstract aspect VisitorProtocol {

	public void Node.accept(Visitor visitor){
		visitor.visit(this);
	}
	
	public void Leaf.accept(Visitor visitor){
		visitor.visit(this);
	}
	
	public void Visitable.accept(Visitor visitor){
		
	}
	
}
