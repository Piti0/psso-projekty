package main.java.visitor;

import main.java.tree.*;

public aspect Visiting extends VisitorProtocol {

	declare parents: NumericNode implements Leaf;
	declare parents: BinaryOperatorNode implements Node;
	declare parents: TreeNode implements Visitable;
	
}
