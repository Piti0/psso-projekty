package main.java.visitors.visitors;

import java.util.Stack;

import main.java.tree.BinaryOperatorNode;
import main.java.tree.NumericNode;
import main.java.visitor.Leaf;
import main.java.visitor.Node;
import main.java.visitor.Visitor;

public class EvaluateVisitor implements Visitor {

    private Stack<Integer> stack = new Stack<>();

    public int getValue(){
        return stack.peek();
    }

    @Override
    public void visit(Node node) {
    	BinaryOperatorNode binaryOperatorNode = (BinaryOperatorNode)node;
        binaryOperatorNode.getLeft().accept(this);
        binaryOperatorNode.getRight().accept(this);
        int right = stack.pop();
        int left = stack.pop();
        stack.push(binaryOperatorNode.compute(left, right));
    }

    @Override
    public void visit(Leaf leaf) {
    	NumericNode numericNode = (NumericNode)leaf;
        stack.push(numericNode.getValue());
    }

    @Override
    public String report() {
        return "Evaluation result: " + getValue();
    }

}
