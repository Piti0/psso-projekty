package main.java.visitors.visitors;

import main.java.tree.BinaryOperatorNode;
import main.java.tree.NumericNode;
import main.java.visitor.*;

public class InFixPrintVisitor implements Visitor {

    private StringBuilder result = new StringBuilder();

    @Override
    public void visit(Node node) {
    	BinaryOperatorNode binaryOperatorNode = (BinaryOperatorNode)node;
        result.append("(");
        binaryOperatorNode.getLeft().accept(this);
        result.append(binaryOperatorNode.getLabel());
        binaryOperatorNode.getRight().accept(this);
        result.append(")");
    }

    @Override
    public void visit(Leaf leaf) {
    	NumericNode numericNode = (NumericNode)leaf;
        result.append(numericNode.getLabel());
    }

    @Override
    public String report() {
        return "InFix notation: " + result.toString();
    }

}
