package main.java.visitors;

import main.java.tree.BinaryOperatorNode;
import main.java.tree.NumericNode;

public abstract class Visitor {

    public abstract void visit(BinaryOperatorNode binaryOperatorNode);
    public abstract void visit(NumericNode numericNode);
    public abstract String report();
    
}
