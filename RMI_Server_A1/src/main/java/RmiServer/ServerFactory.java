package RmiServer;

/**
 * AbstractFactory pattern
 */
public interface ServerFactory {
    IAuctionServer createAuctionServer(String serviceName) throws Exception;
}
