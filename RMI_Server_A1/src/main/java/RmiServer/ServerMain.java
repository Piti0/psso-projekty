package RmiServer;

public class ServerMain {
    public static final String STUB_NAME = "Hello";

    public static void main(String args[]) {

        try {
            ServerFactory serverFactory = getCurrentContextServerFactory();
            IAuctionServer server = serverFactory.createAuctionServer(STUB_NAME);

            server.placeItemForBid("SomeOwner1", "Item1", "Opis", 10, 1000);
            server.placeItemForBid("SomeOwner2", "Item3", "Opis asdfasf", 100, 10000);
            server.placeItemForBid("SomeOwner1", "Item2", "Opis awe32352545343", 1, 500);

            System.err.println("Server ready");
        } catch (Exception e) {
            System.err.println("Server exception: " + e.toString());
            e.printStackTrace();
        }
    }

    private static RmiServerFactory getCurrentContextServerFactory() {
        return new RmiServerFactory();
    }
}
