package RmiServer;

import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

class DiskStoredItemsContainer implements ItemsContainer {
    private final String itemsDir = "./items/";
    private final Gson gson;
    private HashMap<String, Item> items=new HashMap<>();

    public DiskStoredItemsContainer() {
        Path dir = Paths.get(itemsDir);
        if (!Files.isDirectory(dir)) {
            try {
                Files.createDirectory(dir);
            } catch (IOException e) {
            }
        }
        gson = new GsonBuilder().addSerializationExclusionStrategy(new ExclusionStrategy() {
            @Override
            public boolean shouldSkipField(FieldAttributes fieldAttributes) {
                if(fieldAttributes.getName().equals("obs") || fieldAttributes.getName().equals("changed") )
                    return true;
                return false;
            }

            @Override
            public boolean shouldSkipClass(Class<?> aClass) {
                return false;
            }
        }).create();
    }

    @Override
    public Item putItem(String owner, String itemName, String itemDesc, double bid, int auctionTime) {
        Path itemPath = Paths.get(itemsDir + itemName);
        if (Files.exists(itemPath))
            throw new IllegalArgumentException();
        Item newItem = createItem(owner, itemName, itemDesc, bid, auctionTime);
        saveToFile(newItem);

        return applyObserver(newItem);
    }

    protected Item createItem(String owner, String itemName, String itemDesc, double bid, int auctionTime) {
        return new Item(owner, itemName, itemDesc, bid, auctionTime);
    }

    private Item applyObserver(Item newItem) {
        newItem.addObserver((o, arg) -> saveToFile((Item)o));
        items.put(newItem.getName(), newItem);
        return newItem;
    }

    private void saveToFile(Item newItem) {
        Path itemPath = Paths.get(itemsDir+newItem.getName());
        try {
        if (Files.exists(itemPath))
            Files.delete(itemPath);
        String json = gson.toJson(newItem);
            Files.write(itemPath, json.getBytes());
        } catch (IOException e) {
        }
    }

    @Override
    public Item[] getAllItems() {
        Path dir = Paths.get(itemsDir);
        List<Item> cachedItems = new ArrayList<>( items.values());
        try {
            cachedItems.addAll(Files.list(dir)
                    .filter(path -> Files.isRegularFile(path))
                    .map(path -> {
                        try {
                            String filename = path.getFileName().toString();
                            if(items.containsKey(filename))
                                return null;
                            return Files.readAllLines(path);
                        } catch (IOException e) {
                        }
                        return null;
                    }).filter(Objects::nonNull)
                    .map(strings -> {
                        StringBuilder result = new StringBuilder();
                        strings.forEach(result::append);
                        return result.toString();
                    })
                    .map(string -> applyObserver(gson.fromJson(string, Item.class)))
                    .collect(Collectors.toList()));
            return cachedItems.toArray(new Item[0]);
        } catch (IOException e) {
        }
        return null;
    }

    @Override
    public Item getItem(String name) {
        Path itemPath = Paths.get(itemsDir + name);
        if(items.containsKey(name))
            return items.get(name);
        if (!Files.exists(itemPath))
            return null;
        else {
            try {
                StringBuilder result = new StringBuilder();
                Files.readAllLines(itemPath).forEach(result::append);
                return applyObserver(gson.fromJson(result.toString(), Item.class));
            } catch (IOException e) {
                return null;
            }
        }
    }
}
