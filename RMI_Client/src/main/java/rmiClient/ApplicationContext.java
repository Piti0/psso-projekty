package rmiClient;

import RmiServer.IAuctionListener;
import RmiServer.Item;
import javafx.collections.FXCollections;
import javafx.collections.ObservableMap;
import lombok.Data;
import rmiClient.model.AuctionListener;
import rmiClient.model.AuctionServerStub;
import rmiClient.model.biddingStrategy.strategies.NoneStrategy;
import rmiClient.model.biddingStrategy.strategies.Strategy;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

@Data
public class ApplicationContext {

    private static ApplicationContext instance = null;

    private Map<String, AuctionListener> listenersRegistered = new HashMap<>();
    private Map<String, Strategy> itemsBidStrategies = new HashMap<>();
    private String userName;
    private ObservableMap<String, Item> items = FXCollections.observableHashMap();
    private boolean schedulerInitialized = false;

    private ApplicationContext(){
        initItemsData();
    }

    public static ApplicationContext getInstance(){
        if(instance == null){
            instance = new ApplicationContext();
        }
        return instance;
    }

    public void initItemsData() {
        items.clear();
        try {
            Item[] itemsFromServer = AuctionServerStub.getStub().getItems();
            for(int i = 0; i < itemsFromServer.length; i++){
                Item itemIter = itemsFromServer[i];
                items.put(itemIter.getName(), itemIter);
                if(!isListenerRegistered(itemIter.getName())) {
                    AuctionListener listener = new AuctionListener(items);
                    AuctionServerStub.getStub().registerListener(wrapListener(listener), itemIter.getName());
                    listenerRegisteredFor(itemIter.getName(), listener);
                }
                itemsBidStrategies.put(itemIter.getName(), new NoneStrategy());
            }
            if(!schedulerInitialized) {
                schedulerInitialized = true;
                new Timer().schedule(
                        new TimerTask() {

                            @Override
                            public void run() {
                                for (Item item : items.values()) {
                                    itemsBidStrategies.get(item.getName()).timingTask(item);
                                }
                            }
                        }, 0, 1000);
            }
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    public void setStrategy(String itemName, Strategy strategy){
        itemsBidStrategies.put(itemName, strategy);
    }

    public Strategy getStrategy(String itemName){
        return itemsBidStrategies.get(itemName);
    }

    private IAuctionListener wrapListener(AuctionListener listener) throws RemoteException {
        return (IAuctionListener) UnicastRemoteObject.exportObject(listener, 0);
    }

    public void listenerRegisteredFor(String itemName, AuctionListener listener){
        listenersRegistered.put(itemName, listener);
    }

    public boolean isListenerRegistered(String itemName){
        return listenersRegistered.containsKey(itemName);
    }

}
