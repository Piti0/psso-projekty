package rmiClient;


import javafx.scene.control.TextField;

public class Util {

    public static void makeTextFieldNumericOnly(TextField textField){
        textField.textProperty().addListener((observable, oldValue, newValue) -> {
            if (!newValue.matches("\\d*")) {
                textField.setText(newValue.replaceAll("[^\\d]", ""));
            }
        });
    }

}
