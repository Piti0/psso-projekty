package rmiClient.scenesManage;

public enum ScenesNames {
    ITEMS, START, NEW_ITEM;

    public static String getFileName(ScenesNames name){
        if(name.equals(ITEMS)){
            return "itemsView.fxml";
        } else if(name.equals(START)){
            return "startView.fxml";
        } else if(name.equals(NEW_ITEM)){
            return "newItemView.fxml";
        }

        throw new NoSuchSceneException(name);
    }

}
