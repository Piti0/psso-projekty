package rmiClient;

import javafx.application.Application;
import javafx.stage.Stage;
import rmiClient.scenesManage.ScenesManager;
import rmiClient.scenesManage.ScenesNames;

public class Main extends Application {

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) {
        ScenesManager.setStage(primaryStage);
        ScenesManager.setScene(ScenesNames.START);
        primaryStage.setTitle("Bids");
        primaryStage.show();
    }
}