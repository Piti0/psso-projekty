package rmiClient.controller;

import javafx.scene.control.TextField;
import rmiClient.ApplicationContext;
import rmiClient.Util;
import rmiClient.model.AuctionServerStub;
import rmiClient.scenesManage.ScenesManager;
import rmiClient.scenesManage.ScenesNames;

import java.rmi.RemoteException;

public class NewItemController {
    public TextField ownerNameTextField;
    public TextField itemNameTextField;
    public TextField descriptionTextField;
    public TextField startBidTextField;
    public TextField auctionTimeTextField;

    public void initialize(){
        Util.makeTextFieldNumericOnly(startBidTextField);
        Util.makeTextFieldNumericOnly(auctionTimeTextField);
        ownerNameTextField.setText(ApplicationContext.getInstance().getUserName());
        ownerNameTextField.setEditable(false);
        itemNameTextField.setText(ApplicationContext.getInstance().getUserName() + "Item");
        descriptionTextField.setText(ApplicationContext.getInstance().getUserName() + "Description");
        startBidTextField.setText("1");
        auctionTimeTextField.setText("10");
    }

    public void confirm() {
        try {
            AuctionServerStub.getStub().placeItemForBid(ApplicationContext.getInstance().getUserName(),
                                                        itemNameTextField.getText(),
                                                        descriptionTextField.getText(),
                                                        Double.parseDouble(startBidTextField.getText()),
                                                        Integer.parseInt(auctionTimeTextField.getText()));
        } catch (RemoteException e) {
            e.printStackTrace();
        }
        ApplicationContext.getInstance().initItemsData();
        ScenesManager.setScene(ScenesNames.ITEMS);
    }
}
