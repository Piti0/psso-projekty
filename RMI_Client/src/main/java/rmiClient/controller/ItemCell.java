package rmiClient.controller;

import RmiServer.Item;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import rmiClient.ApplicationContext;
import rmiClient.Util;
import rmiClient.model.AuctionServerStub;
import rmiClient.model.biddingStrategy.BiddingStrategies;
import rmiClient.model.biddingStrategy.strategies.LastMinuteStrategy;
import rmiClient.model.biddingStrategy.strategies.MaxBidStrategy;
import rmiClient.model.biddingStrategy.strategies.NoneStrategy;
import rmiClient.model.biddingStrategy.strategies.Strategy;

import java.rmi.RemoteException;

public class ItemCell extends ListCell<Item> {

    private Label nameLabel;
    private TextArea infoLabel;
    private TextField bidValueTextField;
    private Button bidButton;
    private ComboBox<BiddingStrategies> strategyComboBox;
    private TextField maxBidField;
    private Label strategyLabel;
    private VBox strategyVBox;
    private HBox hBox;

    public ItemCell(){
        super();

        initNameLabel();
        initInfoLabel();
        initBidValueTextField();
        initBidButton();
        initStrategyControls();
        hBox = new HBox();
        hBox.getChildren().addAll(nameLabel, infoLabel, bidValueTextField, bidButton, strategyVBox);
        hBox.setAlignment(Pos.CENTER);
        setText(null);
    }

    private void initStrategyControls() {
        initStrategyComboBox();
        initMaxBidField();
        Button useStrategyButton = initConfirmStrategyButton();
        initStrategyLabel();
        strategyVBox = new VBox();
        strategyVBox.getChildren().addAll(strategyComboBox, maxBidField, useStrategyButton, strategyLabel);
        strategyVBox.setAlignment(Pos.CENTER);
    }

    private void initStrategyLabel() {
        strategyLabel = new Label();
        strategyLabel.setText(BiddingStrategies.NONE.toString());
    }

    private Button initConfirmStrategyButton() {
        Button useStrategyButton = new Button();
        useStrategyButton.setText("Confirm strategy");
        useStrategyButton.setOnAction(event ->
        {
            Strategy strategy = null;
            BiddingStrategies selectedStrategy = strategyComboBox.getSelectionModel().getSelectedItem();
            strategyLabel.setText(selectedStrategy.toString());
            switch(selectedStrategy){
                case NONE:
                    strategy = new NoneStrategy();
                    break;
                case LAST_MINUTE:
                    strategy = new LastMinuteStrategy();
                    break;
                case MAX_BID:
                    strategy = new MaxBidStrategy(Double.parseDouble(maxBidField.getText()));
                    break;
            }
            ApplicationContext.getInstance().setStrategy(nameLabel.getText(), strategy);
        });
        return useStrategyButton;
    }

    private void initMaxBidField() {
        maxBidField = new TextField();
        Util.makeTextFieldNumericOnly(maxBidField);
        maxBidField.setPrefWidth(40);
        maxBidField.setVisible(false);
    }

    private void initStrategyComboBox() {
        strategyComboBox = new ComboBox<>();
        strategyComboBox.getItems().addAll(BiddingStrategies.values());
        strategyComboBox.getSelectionModel().selectFirst();
        strategyComboBox.valueProperty().addListener(
                (options, oldValue, newValue) -> {
                    switch(newValue){
                        case NONE:
                            maxBidField.setVisible(false);
                            bidButton.setDisable(false);
                            break;
                        case LAST_MINUTE:
                            maxBidField.setVisible(false);
                            bidButton.setDisable(true);
                            break;
                        case MAX_BID:
                            maxBidField.setVisible(true);
                            bidButton.setDisable(true);
                            break;
                    }
                }
        );
    }

    private void initInfoLabel() {
        infoLabel = new TextArea();
        infoLabel.setPrefColumnCount(23);
        infoLabel.setWrapText(true);
        infoLabel.setPrefRowCount(7);
        infoLabel.setEditable(false);
    }

    private void initBidButton() {
        bidButton = new Button("Bid");
        bidButton.setOnAction(event ->
        {
            try {
                AuctionServerStub.getStub().bidOnItem(ApplicationContext.getInstance().getUserName(),
                                                        nameLabel.getText(),
                                                        Integer.parseInt(bidValueTextField.getText()));
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        });
    }

    private void initNameLabel() {
        nameLabel = new Label();
        nameLabel.setPrefWidth(50);
    }

    private void initBidValueTextField() {
        bidValueTextField = new TextField();
        Util.makeTextFieldNumericOnly(bidValueTextField);
        bidValueTextField.setPrefWidth(40);
    }

    @Override
    public void updateItem(Item item, boolean empty) {
        super.updateItem(item, empty);
        setEditable(false);
        if (item != null) {
            nameLabel.setText(item.getName());
            infoLabel.setText("Owner: " + item.getOwner() + "\n" +
                    "Description: " + item.getDesc() + "\n" +
                    "Current bid: " + item.getBid() + "\n" +
                    "Last bidder: " + item.getLastBidder() + "\n" +
                    "Auction time: " + item.getAuctionTime() + "\n" +
                    "Last update: " + item.getLastUpdate() + "\n");
            if(item.isActual()){
                hBox.setBackground(new Background(new BackgroundFill(Color.GREENYELLOW, CornerRadii.EMPTY, Insets.EMPTY)));
                bidButton.setDisable(false);
            } else {
                hBox.setBackground(new Background(new BackgroundFill(Color.INDIANRED, CornerRadii.EMPTY, Insets.EMPTY)));
                bidButton.setDisable(true);
            }
            bidValueTextField.setText("");
            setGraphic(hBox);
        } else {
            setGraphic(null);
        }
    }

}
