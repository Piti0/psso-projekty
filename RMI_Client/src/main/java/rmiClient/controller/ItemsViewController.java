package rmiClient.controller;

import RmiServer.Item;
import javafx.collections.FXCollections;
import javafx.collections.MapChangeListener;
import javafx.collections.ObservableList;
import javafx.collections.ObservableMap;
import javafx.scene.control.ListView;
import rmiClient.ApplicationContext;
import rmiClient.scenesManage.ScenesManager;
import rmiClient.scenesManage.ScenesNames;

public class ItemsViewController {
    public ListView<Item> itemsListView;

    public void initialize(){
        ObservableMap<String, Item> items = ApplicationContext.getInstance().getItems();
        itemsListView.setCellFactory(lv -> new ItemCell());
        ObservableList<Item> itemsList = FXCollections.observableArrayList(items.values());
        synchronizeListViewWithMap(itemsList, items);
        itemsListView.setItems(itemsList.sorted((o1, o2) -> o1.getName().compareToIgnoreCase(o2.getName())));
    }

    private void synchronizeListViewWithMap(ObservableList<Item> itemsList,
                                            ObservableMap<String, Item> items) {
        items.addListener((MapChangeListener.Change<? extends String, ? extends Item> c) -> {
            if(c.wasAdded() && c.wasRemoved()){
                int index = itemsList.indexOf(c.getValueRemoved());
                itemsList.remove(index);
                itemsList.add(index, c.getValueAdded());
            } else {
                if (c.wasAdded()) {
                    itemsList.add(c.getValueAdded());
                } else if (c.wasRemoved()) {
                    itemsList.remove(c.getValueRemoved());
                }
            }
            itemsListView.refresh();
        });
    }

    public void addNewItem() {
        ScenesManager.setScene(ScenesNames.NEW_ITEM);
    }

    public void refreshList() {
        ApplicationContext.getInstance().initItemsData();
    }
}
