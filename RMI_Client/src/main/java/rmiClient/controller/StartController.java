package rmiClient.controller;

import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import rmiClient.ApplicationContext;
import rmiClient.scenesManage.ScenesManager;
import rmiClient.scenesManage.ScenesNames;

public class StartController {
    public Button confirmButton;
    public TextField nameTextField;

    public void confirm() {
        ApplicationContext.getInstance().setUserName(nameTextField.getText());
        if(!ApplicationContext.getInstance().getUserName().isEmpty())
            ScenesManager.setScene(ScenesNames.ITEMS);
    }
}
