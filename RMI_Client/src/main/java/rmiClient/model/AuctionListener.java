package rmiClient.model;

import RmiServer.IAuctionListener;
import RmiServer.Item;
import javafx.application.Platform;
import javafx.collections.ObservableMap;
import rmiClient.ApplicationContext;

import java.rmi.RemoteException;

public class AuctionListener implements IAuctionListener {

    private ObservableMap<String, Item> items;

    public AuctionListener(ObservableMap<String, Item> items){
        this.items = items;
    }

    @Override
    public void update(Item item) throws RemoteException {
        Platform.runLater(
                () -> {
                    items.put(item.getName(), item);
                    ApplicationContext.getInstance().getStrategy(item.getName()).reactionForBid(item);
                }
        );
    }

}
