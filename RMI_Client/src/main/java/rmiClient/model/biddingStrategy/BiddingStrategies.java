package rmiClient.model.biddingStrategy;

public enum BiddingStrategies {
    NONE, LAST_MINUTE, MAX_BID
}
