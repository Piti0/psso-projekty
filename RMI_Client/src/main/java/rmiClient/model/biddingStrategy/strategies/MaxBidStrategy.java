package rmiClient.model.biddingStrategy.strategies;

import RmiServer.Item;
import rmiClient.ApplicationContext;
import rmiClient.model.AuctionServerStub;

import java.rmi.RemoteException;

public class MaxBidStrategy implements Strategy {

    private double maxBid;

    public MaxBidStrategy(double maxBid){
        this.maxBid = maxBid;
    }


    @Override
    public void timingTask(Item item) {

    }

    @Override
    public void reactionForBid(Item item) {
        String userName = ApplicationContext.getInstance().getUserName();
        String lastBidder = item.getLastBidder();
        if(!lastBidder.equals(userName) &&
            item.getBid() + 1 <= maxBid &&
            item.isActual()){
            try {
                AuctionServerStub.getStub().bidOnItem(userName,
                        item.getName(),
                        item.getBid() + 1);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
    }
}
