package rmiClient.model.biddingStrategy.strategies;

import RmiServer.Item;

public class NoneStrategy implements Strategy {
    @Override
    public void timingTask(Item item) {

    }

    @Override
    public void reactionForBid(Item item) {

    }
}
