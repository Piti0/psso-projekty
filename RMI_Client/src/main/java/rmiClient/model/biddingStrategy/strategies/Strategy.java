package rmiClient.model.biddingStrategy.strategies;

import RmiServer.Item;

public interface Strategy {
    void timingTask(Item item);
    void reactionForBid(Item item);
}
