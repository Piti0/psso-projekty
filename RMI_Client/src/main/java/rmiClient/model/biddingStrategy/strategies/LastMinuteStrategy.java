package rmiClient.model.biddingStrategy.strategies;

import RmiServer.Item;
import rmiClient.ApplicationContext;
import rmiClient.model.AuctionServerStub;

import java.rmi.RemoteException;
import java.time.Instant;

public class LastMinuteStrategy implements Strategy {

    private boolean wasBidded = false;
    private static final int TIME_TO_END = 60;

    @Override
    public void timingTask(Item item) {
        if(item.isActual() &&
            item.getLastUpdate().toInstant().getEpochSecond() + item.getAuctionTime() - Instant.now().getEpochSecond() <= TIME_TO_END &&
            !wasBidded){
            wasBidded = true;
            try {
                AuctionServerStub.getStub().bidOnItem(ApplicationContext.getInstance().getUserName(),
                        item.getName(),
                        item.getBid() * 2);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void reactionForBid(Item item) {

    }
}
