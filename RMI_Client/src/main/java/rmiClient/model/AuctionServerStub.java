package rmiClient.model;

import RmiServer.IAuctionServer;

import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

public class AuctionServerStub {
    public static final String STUB_NAME = "Hello";
    public static final String HOST = "localhost";

    private static IAuctionServer stub;

    public static IAuctionServer getStub() {
        if (stub == null){
            Registry registry;
            try {
                registry = LocateRegistry.getRegistry(HOST);
                stub = (IAuctionServer) registry.lookup(STUB_NAME);
            } catch (RemoteException | NotBoundException e) {
                e.printStackTrace();
            }
        }
        return stub;
    }
}
