package main.java.noVisitor;
//abstract superclass of tree element types

public abstract class TreeNode {

    protected String label;

    protected TreeNode(String label) {
        this.label = label;
    }

    public abstract String inFixPrint();

    public abstract String preFixPrint();

    public abstract String postFixPrint();

    public abstract double evaluate();
}