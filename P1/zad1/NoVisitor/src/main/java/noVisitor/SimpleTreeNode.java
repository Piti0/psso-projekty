package main.java.noVisitor;
//Define tree element with no children

public class SimpleTreeNode extends TreeNode {

    public SimpleTreeNode(String label) {
        super(label);
    }

    public String inFixPrint() {
        return " "+label+" ";
    }

    @Override
    public String preFixPrint() {
        return inFixPrint();
    }

    @Override
    public String postFixPrint() {
        return inFixPrint();
    }

    @Override
    public double evaluate() {
        return Integer.parseInt(label);
    }

}