package main.java.noVisitor;
//Tree node that contains two children

import java.util.List;
import java.util.function.BiFunction;

public class BinaryTreeNode extends TreeNode {
    private List<Pair<String, BiFunction<Double, Double, Double>>> operators;
    private TreeNode left, right;

    public BinaryTreeNode(String label, List<Pair<String, BiFunction<Double, Double, Double>>> operators) {
        super(label);
        left = right = null;
        this.operators = operators;
    }

    public BinaryTreeNode(String label, TreeNode left, TreeNode right, List<Pair<String, BiFunction<Double, Double, Double>>> operators) {
        super(label);
        this.left = left;
        this.right = right;
        this.operators = operators;
    }

    @Override
    public String inFixPrint() {
        return " ( " + left.inFixPrint() + " " + label + " " + right.inFixPrint() + " ) ";
    }

    @Override
    public String preFixPrint() {
        return " ( " + label + " " + left.preFixPrint() + " " + right.preFixPrint() + " ) ";
    }

    @Override
    public String postFixPrint() {
        return " ( " + left.preFixPrint() + " " + right.preFixPrint() + " " + label + " ) ";
    }

    @Override
    public double evaluate() {
        for(var operator:operators){
            if(operator.first.equals(label))
                return operator.second.apply(left.evaluate(), right.evaluate());
        }
        throw new IllegalArgumentException();
    }

}