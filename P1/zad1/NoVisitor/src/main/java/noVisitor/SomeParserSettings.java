package main.java.noVisitor;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.BiFunction;

public class SomeParserSettings implements ParserSettings {
    private List<Pair<String, Integer>> operators;
    private List<Pair<String, BiFunction<Double, Double, Double>>> operatorsFuncs;

    public SomeParserSettings() {
    }

    public void initStandardData(){
        operators = new ArrayList<>(Arrays.asList(new Pair<>("+", 1),
                new Pair<>("-", 1),
                new Pair<>("*", 2),
                new Pair<>("/", 2)));
        operatorsFuncs = new ArrayList<>();
        operatorsFuncs.add(new Pair<>("-", (a, b) -> a - b));
        operatorsFuncs.add(new Pair<>("+", (a, b) -> a + b));
        operatorsFuncs.add(new Pair<>("/", (a, b) -> a / b));
        operatorsFuncs.add(new Pair<>("*", (a, b) -> a * b));
    }

    public void addOperator(String operator, int priority, BiFunction<Double, Double, Double> evaluate){
        operators.add(new Pair<>(operator, priority));
        operatorsFuncs.add(new Pair<>(operator, evaluate));
    }

    @Override
    public boolean isClosingBracket(String token) {
        return token.equals(")");
    }

    @Override
    public boolean isOpeningBracket(String token) {
        return token.equals("(");
    }

    @Override
    public List<Pair<String, BiFunction<Double, Double, Double>>> getOperatorsFuncs() {
        return operatorsFuncs;
    }

    @Override
    public int getOperatorPriority(String op) {
        for (Pair<String, Integer> operator : operators)
            if (operator.first.equals(op))
                return operator.second;
        return -1;
    }

    @Override
    public boolean isValue(String integer) {
        try {
            Integer.parseInt(integer);
            return true;
        } catch (NumberFormatException nfe) {
            return false;
        }
    }

    @Override
    public boolean isOperator(String op) {
        for (Pair<String, Integer> operator : operators)
            if (operator.first.equals(op)) return true;
        return false;
    }
}
