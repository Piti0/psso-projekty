package main.java.noVisitor;
import java.util.List;
import java.util.function.BiFunction;

public interface ParserSettings {
    boolean isClosingBracket(String token);

    boolean isOpeningBracket(String token);

    List<Pair<String, BiFunction<Double, Double, Double>>> getOperatorsFuncs();

    int getOperatorPriority(String op);

    boolean isValue(String integer);

    boolean isOperator(String op);
}
