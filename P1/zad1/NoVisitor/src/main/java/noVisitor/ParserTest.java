package main.java.noVisitor;
public class ParserTest {
    public static void main(String[] args) {
        var parserSettings=new SomeParserSettings();
        parserSettings.initStandardData();
        Parser parser = new Parser(parserSettings);
        TreeNode rootNode = parser.parse("1 + ( 2 - 3 * 4 ) / 5");// spaces are vital!!;

        System.out.println(rootNode.inFixPrint());
        System.out.println(rootNode.preFixPrint());
        System.out.println(rootNode.postFixPrint());
        System.out.println("result: " + rootNode.evaluate());


        parserSettings.addOperator("%", 4, (a,b)->a%b);
        
        TreeNode rootNode2 = parser.parse("2 * ( 7 * 3 % 2 ) + ( 2 - 3 * 4 ) / 5");// spaces are vital!!;
        System.out.println(rootNode2.inFixPrint());
        System.out.println(rootNode2.preFixPrint());
        System.out.println(rootNode2.postFixPrint());
        System.out.println("result: " + rootNode2.evaluate());
    }
}