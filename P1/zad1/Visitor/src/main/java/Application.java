package main.java;

import main.java.parser.Parser;
import main.java.parser.SomeParserSettings;
import main.java.tree.TreeNode;
import main.java.tree.operatorNodes.ModuloNode;
import main.java.visitor.visitors.EvaluateVisitor;
import main.java.visitor.visitors.InFixPrintVisitor;
import main.java.visitor.visitors.PostFixPrintVisitor;
import main.java.visitor.visitors.PreFixPrintVisitor;

public class Application {

    public static void main(String[] args) {
        SomeParserSettings parserSettings = new SomeParserSettings();
        Parser parser = new Parser(parserSettings);
        TreeNode rootNode = parser.parse("1 + ( 2 - 3 * 4 ) / 5");// spaces are vital!!
        fullTreeReport(rootNode);

        parserSettings.addOperator("%", 2, ModuloNode.class);
        TreeNode rootNode2 = parser.parse("2 * ( 7 * 3 % 2 ) + ( 2 - 3 * 4 ) / 5");// spaces are vital!!
        fullTreeReport(rootNode2);

        TreeNode rootNode3 = parser.parse("2 + 2 * 2 % 3");// spaces are vital!!
        fullTreeReport(rootNode3);

        TreeNode rootNode4 = parser.parse("( 2 + 2 ) * 2 % 3");// spaces are vital!!
        fullTreeReport(rootNode4);
    }

    private static void fullTreeReport(TreeNode rootNode) {
        PreFixPrintVisitor preFixPrintVisitor = new PreFixPrintVisitor();
        rootNode.accept(preFixPrintVisitor);
        System.out.println(preFixPrintVisitor.report());

        InFixPrintVisitor inFixPrintVisitor = new InFixPrintVisitor();
        rootNode.accept(inFixPrintVisitor);
        System.out.println(inFixPrintVisitor.report());

        PostFixPrintVisitor postFixPrintVisitor = new PostFixPrintVisitor();
        rootNode.accept(postFixPrintVisitor);
        System.out.println(postFixPrintVisitor.report());

        EvaluateVisitor evaluateVisitor = new EvaluateVisitor();
        rootNode.accept(evaluateVisitor);
        System.out.println(evaluateVisitor.report());

        System.out.println();
    }

}
