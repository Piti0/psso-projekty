package main.java.tree;

import main.java.visitor.Visitor;

public class NumericNode implements TreeNode {

    private int value;

    public NumericNode(int value){
        this.value = value;
    }

    @Override
    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    @Override
    public String getLabel() {
        return Integer.toString(value);
    }

    public int getValue(){
        return value;
    }
}
