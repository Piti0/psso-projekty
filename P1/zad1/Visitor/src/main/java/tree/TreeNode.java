package main.java.tree;

import main.java.visitor.Visitor;

public interface TreeNode {

    void accept(Visitor visitor);
    String getLabel();

}
