package main.java.tree;

import main.java.visitor.Visitor;

public abstract class BinaryOperatorNode implements TreeNode {

    private TreeNode left;
    private TreeNode right;

    public BinaryOperatorNode(){
        left = null;
        right = null;
    }

    public BinaryOperatorNode(TreeNode left, TreeNode right) {
        this.left = left;
        this.right = right;
    }

    public abstract int compute(int a, int b);

    public TreeNode getLeft() {
        return left;
    }

    public TreeNode getRight() {
        return right;
    }

    @Override
    public void accept(Visitor visitor) {
        visitor.visit(this);
    }
}
