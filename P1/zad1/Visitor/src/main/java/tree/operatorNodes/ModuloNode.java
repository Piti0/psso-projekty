package main.java.tree.operatorNodes;

import main.java.tree.BinaryOperatorNode;
import main.java.tree.TreeNode;

public class ModuloNode extends BinaryOperatorNode {

    public ModuloNode(TreeNode left, TreeNode right){
        super(left, right);
    }

    @Override
    public int compute(int a, int b) {
        return a % b;
    }

    @Override
    public String getLabel() {
        return "%";
    }
}
