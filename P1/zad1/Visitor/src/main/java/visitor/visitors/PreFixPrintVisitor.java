package main.java.visitor.visitors;

import main.java.tree.BinaryOperatorNode;
import main.java.tree.NumericNode;
import main.java.visitor.Visitor;

public class PreFixPrintVisitor extends Visitor {

    private StringBuilder result = new StringBuilder();

    @Override
    public void visit(BinaryOperatorNode binaryOperatorNode) {
        result.append(binaryOperatorNode.getLabel());
        binaryOperatorNode.getLeft().accept(this);
        binaryOperatorNode.getRight().accept(this);
    }

    @Override
    public void visit(NumericNode numericNode) {
        result.append(numericNode.getLabel());
    }

    @Override
    public String report() {
        return "PreFix notation: " + result.toString();
    }

}
