package main.java.visitor.visitors;

import main.java.tree.BinaryOperatorNode;
import main.java.tree.NumericNode;
import main.java.visitor.Visitor;

public class PostFixPrintVisitor extends Visitor {

    private StringBuilder result = new StringBuilder();

    @Override
    public void visit(BinaryOperatorNode binaryOperatorNode) {
        binaryOperatorNode.getLeft().accept(this);
        binaryOperatorNode.getRight().accept(this);
        result.append(binaryOperatorNode.getLabel());
    }

    @Override
    public void visit(NumericNode numericNode) {
        result.append(numericNode.getLabel());
    }

    @Override
    public String report() {
        return "PostFix notation: " + result.toString();
    }

}
