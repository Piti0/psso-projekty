package main.java.visitor.visitors;

import java.util.Stack;

import main.java.tree.BinaryOperatorNode;
import main.java.tree.NumericNode;
import main.java.visitor.Visitor;

public class EvaluateVisitor extends Visitor {

    private Stack<Integer> stack = new Stack<>();

    public int getValue(){
        return stack.peek();
    }

    @Override
    public void visit(BinaryOperatorNode binaryOperatorNode) {
        binaryOperatorNode.getLeft().accept(this);
        binaryOperatorNode.getRight().accept(this);
        int right = stack.pop();
        int left = stack.pop();
        stack.push(binaryOperatorNode.compute(left, right));
    }

    @Override
    public void visit(NumericNode numericNode) {
        stack.push(numericNode.getValue());
    }

    @Override
    public String report() {
        return "Evaluation result: " + getValue();
    }

}
