package main.java.visitor.visitors;

import main.java.tree.BinaryOperatorNode;
import main.java.tree.NumericNode;
import main.java.visitor.Visitor;

public class InFixPrintVisitor extends Visitor {

    private StringBuilder result = new StringBuilder();

    @Override
    public void visit(BinaryOperatorNode binaryOperatorNode) {
        result.append("(");
        binaryOperatorNode.getLeft().accept(this);
        result.append(binaryOperatorNode.getLabel());
        binaryOperatorNode.getRight().accept(this);
        result.append(")");
    }

    @Override
    public void visit(NumericNode numericNode) {
        result.append(numericNode.getLabel());
    }

    @Override
    public String report() {
        return "InFix notation: " + result.toString();
    }

}
