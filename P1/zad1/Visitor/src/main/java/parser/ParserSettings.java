package main.java.parser;

import main.java.tree.BinaryOperatorNode;
import main.java.tree.TreeNode;

public interface ParserSettings {
    boolean isClosingBracket(String token);

    boolean isOpeningBracket(String token);

    void addOperator(String operator, int priority, Class operatorClass);

    BinaryOperatorNode tryGetOperatorObject(String operator, TreeNode left, TreeNode right);

    int getOperatorPriority(String op);

    boolean isValue(String integer);

    boolean isOperator(String op);
}
