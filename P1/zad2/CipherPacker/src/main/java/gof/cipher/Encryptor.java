package gof.cipher;

public class Encryptor {
	
	private int[] sBox = new int[256];
	private int keyStreamI = 0; 
	private int keyStreamJ = 0; 
	
	public Encryptor(){
		byte[] key = {1, 2, 3, 4, 5};
		initialization(key);
	}
	
	public Encryptor(byte[] key){
		initialization(key);
	}

	public byte encrypt(byte b) {
		return (byte) (b ^ getKeyStreamint());
	}
	
	private void initialization(byte[] key) {
		initializeSBox();
		int[] kBox = new int[sBox.length];
		initializeKBox(kBox, key);
		scrambling(kBox);
	}
	
	private void initializeSBox() {
		for(int i=0; i<sBox.length; i++) {
			sBox[i] = i;
		}
	}
	
	private void initializeKBox(int[] kBox, byte[] key) {
		for(int i=0; i<kBox.length; i++) {
			kBox[i] = Byte.toUnsignedInt(key[i%key.length]);
		}
	}
	
	private void scrambling(int[] kBox) {
		int j = 0;
		for(int i=0; i<sBox.length; i++) {
		    j = (j + sBox[i] + kBox[i]) % sBox.length;
		    swap(sBox, i, j); 
		}
	}
	
	private void swap(int[] array, int i, int j) {
		int temp = array[i];
		array[i] = array[j];
		array[j] = temp;
	}
	
	private int getKeyStreamint() {
		keyStreamI = (keyStreamI + 1) % sBox.length; 
		keyStreamJ = (keyStreamJ + sBox[keyStreamI]) % sBox.length; 
		swap(sBox, keyStreamI, keyStreamJ); 
	    return sBox[ ( sBox[keyStreamI] + sBox[keyStreamJ] ) % sBox.length ];
	}
}
