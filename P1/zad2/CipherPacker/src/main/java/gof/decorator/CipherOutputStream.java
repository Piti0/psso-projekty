package gof.decorator;

import java.io.*;

import gof.cipher.Encryptor;



public class CipherOutputStream extends FilterOutputStream {
	
	private Encryptor encryptor;

	public CipherOutputStream(OutputStream out) {
		super(out);
		encryptor = new Encryptor();
	}
	
	public CipherOutputStream(OutputStream out, byte[] key) {
		super(out);
		encryptor = new Encryptor(key);
	}

	public void write(byte[] b, int off, int len) throws IOException{
		for(int i=off; i<len; i++) {
			b[i] = encryptor.encrypt(b[i]);
		}
		
		out.write(b, off, len);
	}
	
	public void write(byte[] b) throws IOException{
		write(b, 0, b.length);
	}
	
	public void write(int b) throws IOException{
		out.write(encryptor.encrypt((byte) b));
	}
	
}
