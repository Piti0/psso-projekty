package gof.decorator;

import java.io.*;

import gof.cipher.Encryptor;

public class CipherInputStream extends FilterInputStream {
	
	private Encryptor encryptor;

	protected CipherInputStream(InputStream in) {
		super(in);
		encryptor = new Encryptor();
	}
	
	protected CipherInputStream(InputStream in, byte[] key) {
		super(in);
		encryptor = new Encryptor(key);
	}
	
	public int read(byte[] b, int off, int len) throws IOException{
		int result = in.read(b, off, len);
		
		for(int i=0; i<result; i++) {
			b[i] = encryptor.encrypt(b[i]);
		}
		
		return result;
	}

	public int read(byte[] b) throws IOException{
		return read(b, 0, b.length);
	}
	
	public int read() throws IOException{
		int result = in.read();
		
		if(result == -1) {
			return result;
		} else {
			return encryptor.encrypt((byte) result);
		}
	}

}
