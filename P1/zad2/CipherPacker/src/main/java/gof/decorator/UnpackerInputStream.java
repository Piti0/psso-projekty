package gof.decorator;

import java.io.*;

public class UnpackerInputStream extends FilterInputStream {
	
	private int bit;
	private String strBinary = new String("");

	protected UnpackerInputStream(InputStream in) {
		super(in);
		this.bit = 6;
	}
	
	protected UnpackerInputStream(InputStream in, int bit) {
		super(in);
		this.bit = bit;
	}
	
	public int read(byte[] b, int off, int len) throws IOException{
		writeToStrBinary(off, len);
		applyOffset(off);
		return decodeAndReturn(b, len, off);
	}

	public int read(byte[] b) throws IOException{	
		return read(b, 0, b.length);
	}
	
	public int read() throws IOException{
		byte[] singleByte = new byte[1];
		int result = read(singleByte, 0, 1);
		if(result == -1) {
			return -1;
		} else {
			return singleByte[0];
		}
	}
	
	private void writeToStrBinary(int off, int len) throws IOException {
		if(bit*len > strBinary.length()) {
			byte[] input = new byte[len];
			int length = in.read(input, off, len);
			
			String strTemp = new String("");
			int intTemp=0;
			
			for(int i = 0;i<length;i++){         
				if(input[i]<0){
					intTemp = (int)input[i]+256;
				}else
					intTemp = (int)input[i];
				strTemp = Integer.toBinaryString(intTemp);
				while(strTemp.length()%8 != 0){
					strTemp="0"+strTemp;
				}
				strBinary = strBinary+strTemp;
			}
		}
	}
	
	private void applyOffset(int off) {
		if(off != 0) {
			strBinary = strBinary.substring(off*bit, strBinary.length());
		}
	}
	
	private int decodeAndReturn(byte[] b, int len, int off) {
		if(strBinary.length() >= bit) {
			int length = 0;
			Integer tempInt = new Integer(0);
			while(strBinary.length() >= bit && length < (len - off)){
				tempInt = Integer.valueOf(strBinary.substring(0,bit),2);
				strBinary = strBinary.substring(bit, strBinary.length());
				b[length] = (byte) toChar(tempInt.intValue());
				length++;
			}
			
			return length;
		} else {
			return -1;
		}
	}
	
	char toChar(int val){
		char ch = ' ';
		switch(val){
			case 0:ch=' ';break; case 1:ch='a';break;
			case 2:ch='b';break; case 3 :ch='c';break;
			case 4:ch='d';break; case 5 :ch='e';break;
			case 6:ch='f';break; case 7 :ch='g';break;
			case 8:ch='h';break; case 9 :ch='i';break;
			case 10:ch='j';break; case 11:ch='k';break;
			case 12:ch='l';break; case 13:ch='m';break;
			case 14:ch='n';break; case 15:ch='o';break;
			case 16:ch='p';break; case 17:ch='q';break;
			case 18:ch='r';break; case 19:ch='s';break;
			case 20:ch='t';break; case 21:ch='u';break;
			case 22:ch='v';break; case 23:ch='w';break;
			case 24:ch='x';break; case 25:ch='y';break;
			case 26:ch='z';break; case 27:ch='.';break;
			case 28:ch='[';break; case 29:ch=',';break;
			case 30:ch=']';break; case 31 :ch='2';break;
			case 32:ch='A';break; case 33:ch='B';break;
			case 34:ch='C';break; case 35:ch='D';break;
			case 36:ch='E';break; case 37:ch='F';break;
			case 38:ch='G';break; case 39:ch='H';break;
			case 40:ch='I';break; case 41:ch='J';break;
			case 42:ch='K';break; case 43:ch='L';break;
			case 44:ch='M';break; case 45:ch='N';break;
			case 46:ch='O';break; case 47:ch='P';break;
			case 48:ch='Q';break; case 49:ch='R';break;
			case 50:ch='S';break; case 51:ch='T';break;
			case 52:ch='U';break; case 53:ch='V';break;
			case 54:ch='W';break; case 55:ch='0';break;
			case 56:ch='1';break; case 57:ch='3';break;
			case 58:ch='4';break; case 59:ch='5';break;
			case 60:ch='6';break; case 61:ch='7';break;
			case 62:ch='8';break; case 63:ch='9';break;
			default:ch=' ';
		}
		return ch;
	}
	
}
