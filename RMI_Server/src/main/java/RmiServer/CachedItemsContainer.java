package RmiServer;

import java.util.HashMap;

class CachedItemsContainer implements ItemsContainer {
    private HashMap<String, Item> items=new HashMap<>();

    @Override
    public Item putItem(String owner, String itemName, String itemDesc, double bid, int auctionTime){
        if(items.containsKey(itemName))
            throw new IllegalArgumentException();
        Item newItem = new Item(owner, itemName, itemDesc, bid, auctionTime);
        items.put(itemName, newItem);
        return newItem;
    }

    @Override
    public Item[] getAllItems(){
        return items.values().toArray(new Item[items.values().size()]);
    }

    @Override
    public Item getItem(String name){
        return items.get(name);
    }
}
