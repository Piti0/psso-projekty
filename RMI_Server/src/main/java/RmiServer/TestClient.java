package RmiServer;

import javax.rmi.PortableRemoteObject;
import java.io.Serializable;
import java.rmi.NotBoundException;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.Arrays;

class Listener implements IAuctionListener {

    @Override
    public void update(Item item) throws RemoteException {
        System.out.println("Item update: "+item);
    }
}


public class TestClient {

        public static void main(String[] args) {

            String host = (args.length < 1) ? null : args[0];
            try {

                Registry registry = LocateRegistry.getRegistry(host);
                IAuctionServer stub = (IAuctionServer) registry.lookup("Hello");
                IAuctionListener listener = createListener();

                Item[] response = stub.getItems();
                System.out.println("response: " + Arrays.toString(response));
                stub.registerListener(listener, "i1");
                stub.registerListener(listener, "Item2");

                stub.placeItemForBid("afasfsafd", "i1", "Desfgdsfs", 2344, 5);
                stub.bidOnItem("asdf", "i1", 4444);
                stub.bidOnItem("asdf", "i1", 4445);
                stub.bidOnItem("asdf", "i1", 4446);
                stub.bidOnItem("asdf", "Item2", 4446);
            } catch (Exception e) {
                System.err.println("Client exception: " + e.toString());
                e.printStackTrace();
            }
        }

    private static IAuctionListener createListener() throws RemoteException {
            //listener musi być opakowany w takie nichujtwo
        return (IAuctionListener) UnicastRemoteObject.exportObject(new Listener(), 0);
    }
}
